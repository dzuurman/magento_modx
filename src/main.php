<?php

require "../vendor/autoload.php";

/**
 * @param $sku product sku
 * @return array with all product info
 */
function getProductBySku($sku){

$requester = new \magentoConnector\Connector();
$response = $requester->get('products/' . $sku);
return $response;
}


