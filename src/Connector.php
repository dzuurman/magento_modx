<?php

namespace magentoConnector;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use GuzzleHttp\Psr7;

require '../vendor/autoload.php';

class Connector
{

    private $client;

    public function __construct()
    {
        $settings = new Settings();
        $settings = $settings->getSettings();
        $stack = HandlerStack::create();

        $authenticator = new Oauth1(
            [
                'consumer_key' => $settings['CONSUMER_KEY'],
                'consumer_secret' => $settings['CONSUMER_SECRET'],
                'token' => $settings['TOKEN'],
                'token_secret'=> $settings['TOKEN_SECRET']
            ]
        );

        $stack->push($authenticator);
        $this->client = new Client([
            'base_uri' => $settings['API_URI'],
            'handler' => $stack,
            'auth' => 'oauth'
        ]);
    }

    /**
     * Make a GET request to Magento
     *
     * @param string $route
     * @param array $params
     * @return array
     */
    public function get($route, array $params = [])
    {
        try {
            $response = $this->client->get($route, ['query' => $params]);
            $responseBody = json_decode($response->getBody());
            return $responseBody;
        } catch (RequestException $e) {
            $exception = Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $exception = Psr7\str($e->getResponse());
            }
            return $exception;
        }
    }

}
