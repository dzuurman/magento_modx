<?php


namespace magentoConnector;


class Settings
{
  public function getSettings(){
     $settings = parse_ini_file('../config/settings.ini');
     return $settings;
  }
}